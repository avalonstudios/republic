// import then needed Font Awesome functionality
import {dom, library} from '@fortawesome/fontawesome-svg-core';
// import the Facebook and Twitter icons
import {
  faDribbble,
  faFacebookF,
  faGithub,
  faGooglePlusG,
  faInstagram,
  faLinkedinIn,
  faPinterest,
  faRedditAlien,
  faSkype,
  faSlackHash,
  faStackOverflow,
  faTwitter,
  faVk,
  faWhatsapp,
  faYoutube,
} from '@fortawesome/free-brands-svg-icons';
import {faCalendarAlt, faClock, faComments, faEnvelope, faMap} from '@fortawesome/free-regular-svg-icons';
import {
  faAngleUp,
  faBars,
  faBookmark,
  faCaretDown,
  faChevronCircleDown,
  faFolderOpen,
  faGlobe,
  faMapPin,
  faPaperclip,
  faSearch,
  faTag,
  faTimes,
} from '@fortawesome/free-solid-svg-icons';

// add the imported icons to the library
// library.add(fab, far, fas);
library.add(faChevronCircleDown,
  faPaperclip,
  faTimes,
  faFolderOpen,
  faBookmark,
  faBars,
  faCaretDown,
  faSearch,
  faTag,
  faCalendarAlt,
  faClock,
  faAngleUp,
  faMap,
  faFacebookF,
  faTwitter,
  faGooglePlusG,
  faLinkedinIn,
  faInstagram,
  faPinterest,
  faVk,
  faStackOverflow,
  faYoutube,
  faSlackHash,
  faGithub,
  faComments,
  faEnvelope,
  faDribbble,
  faRedditAlien,
  faWhatsapp,
  faSkype,
  faMapPin,
  faGlobe);

// tell FontAwesome to watch the DOM and add the SVGs when it detects icon markup
dom.watch();
