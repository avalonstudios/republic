import * as L from 'leaflet';
import { GestureHandling } from 'leaflet-gesture-handling';

export default {
  init() {
    // JavaScript to be fired on the home page
  },
  finalize() {
  },
};

doMap();
function doMap() {
  let $mapID = $('#mapid');
  if (!$mapID.length) { return; }
  let lat = $mapID.data('lat');
  let lng = $mapID.data('lng');
  let icon = $mapID.data('icon');

  L.Map.addInitHook('addHandler', 'gestureHandling', GestureHandling);

  var map = L.map(
    'mapid',
    {
      gestureHandling: true,
    }
  ).setView([lat, lng], 16);

  L.tileLayer('https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png', {
    maxZoom: 20,
    attribution: '&copy; <a target="_blank" href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
  }).addTo(map);

  if (icon) {
    var agIcon = L.icon({
      iconUrl: icon,
      iconSize: [39, 47],
      iconAnchor: [15, 37],
    });
  }

  setTimeout(function () {
    map.invalidateSize(false);
  }, 400);

  new L.marker([lat, lng], { icon: agIcon }).addTo(map);
}
