import ClipboardJS from 'clipboard/dist/clipboard.min';
export default {
  init() {
    // JavaScript to be fired on all pages
  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired

    let $open = $('.hamburger, .nav-primary, .footer-menu, .menu-underlay, .social-header, .login-register-menu-small');
    let burgerClicked = false;

    $(window).on('load', onLoad);
    $(window).on('resize', onResize);
    $(window).on('scroll', onScroll);
    // $(window).on('beforeunload', onBeforeUnload);
    onBeforeUnload();

    $('.nav-primary li.menu-item-has-children .drop-dropdown').click(function (e) {
      e.preventDefault();

      if ($(window).innerWidth() > 1024) {
        return;
      }

      var $this = $(this);

      if ($this.next().hasClass('show')) {
        $this.next().removeClass('show');
        $this.next().slideUp(350);
      } else {
        $this.parent().parent().find('.sub-menu').removeClass('show');
        $this.parent().parent().find('.sub-menu').slideUp(350);
        $this.next().toggleClass('show');
        $this.next().slideToggle(350);
      }
    });

    $('.hamburger, .menu-underlay').on('click', () => {
      $open.toggleClass('hide show');

      if (!burgerClicked) {
        burgerClicked = true;
        $('body').css(
          {
            'position': 'fixed',
            'top': 0,
            'width': '100%',
          }
        );
      } else {
        burgerClicked = false;
        $('body').css(
          {
            'position': '',
            'top': '',
            'width': '',
          }
        );
      }
    });

    $('.announcements-toggle').on('click', () => {
      $('.announcements-wrapper').toggleClass('hide show');
      announcementsOpen = !announcementsOpen;
      doAnnouncementsSlider(announcementsOpen);
    });

    $('.search-menu-btn').on('click', () => {
      $('.header-menu-search').toggleClass('hide show');
    });

    $('.header-menu-search .search-close').on('click', () => {
      $('.header-menu-search').toggleClass('hide show');
    });

    $('.scroll-next-screen').on('click', function (e) {
      e.preventDefault();
      console.log('clicked')
      let viewportHeight = $(window).height();

      $('html, body').animate({
        scrollTop: viewportHeight,
      });
    });

    let numOfSections = $('.main section').length;
    if (numOfSections <= 1) {
      $('.scroll-next-screen').hide();
    }

    let clipBoard = new ClipboardJS('.copy-btn');

    clipBoard.on('success', (e) => {
      $('.tooltip-text').fadeOut();
      $(e.trigger).next().show();
      e.clearSelection();
      console.log(e)
    });

    setTimeout(() => {
      stickFooter();
    }, 250);
  },
};

let announcementsOpen = false;

function onResize() {
  stickFooter();
  let wW = $(window).outerWidth();

  if (wW > 1024) {
    $('.nav-primary .sub-menu.show').removeClass('show').css('display', '');
    // $('.social-header').removeClass('show').addClass('hide');
  }
}

function onLoad() {
  $('.preloader-wrapper').fadeOut();
  scrolling();
}

function onBeforeUnload() {
  $('body a[href]').click(function () {
    console.log('fading out');

    var $anchor = $(this);

    // DON'T FADE FOR LINKS THAT OPEN IN NEW WINDOW
    if ($anchor.attr('target') && $anchor.attr('target').indexOf('_blank') >= 0)
      return;

    // DON'T FADE FOR EMAIL
    if ($anchor.attr('href').indexOf('mailto:') >= 0)
      return;

    // DON'T FADE FOR TELEPHONE LINKS
    if ($anchor.attr('href').indexOf('tel:') >= 0)
      return;

    // DON'T FADE FOR LINKS TO ANCHOR TAGS
    if ($anchor.attr('href').indexOf('#') >= 0)
      return;

    // FADE OUT
    $('body').animate({ opacity: 0 }, 500);

  });
}

function onScroll() {
  /* if ($('.announcements-wrapper').hasClass('show')) {
    $('.announcements-wrapper').removeClass('show').addClass('hide');
    announcementsOpen = false;
    doAnnouncementsSlider(announcementsOpen);
  } */

  scrolling();
}

function scrolling() {
  let scrollTop = $(window).scrollTop();

  if (scrollTop > 0) {
    $('.banner').addClass('scrolled');
    $('#back-to-top').addClass('scrolled');
  } else {
    $('.banner').removeClass('scrolled');
    $('#back-to-top').removeClass('scrolled');
  }
}

function doAnnouncementsSlider(isOpen) {
  let settings = {
    dots: true,
    arrows: false,
    infinite: true,
    mobileFirst: true,
    slidesToShow: 1,
    slidesToScroll: 1,
  };

  if (isOpen) {
    $('.announcements-slider').slick(settings);
  } else {
    $('.announcements-slider').slick('unslick');
  }
}

function stickFooter() {
  let viewportHeight = $(window).height();
  let headerHeight = $('.banner').outerHeight();
  let contentHeight = $('.wrap.container').outerHeight();
  let totalHeight = ( headerHeight + contentHeight ) + 62;

  if (totalHeight < viewportHeight) {
    $('.content-info').addClass('fixed');
  } else {
    $('.content-info').removeClass('fixed');
  }
}
