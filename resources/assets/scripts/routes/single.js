export default {
  init() {
    // JavaScript to be fired on the home page
  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
    setTimeout(() => {
      // doRelatedPosts();
    }, 250);
  },
};

/* function doRelatedPosts() {
  let settings = {
    dots: false,
    arrows: true,
    infinite: true,
    mobileFirst: true,
    responsive: [
      {
        breakpoint: 1,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1,
        },
      },
    ],
  };

  $('.related-posts-slider').slick(settings);
}
 */
