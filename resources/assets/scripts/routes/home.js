export default {
  init() {
    // JavaScript to be fired on the home page
  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
    $(window).on('resize', onResize);

    setTimeout(() => {
      bannerHeight();
    }, 500);

    doRelatedPosts();

  },
};

function onResize() {
  bannerHeight();
}

function bannerHeight() {
  var bannerHeight = $('.banner').outerHeight() + 50;
  // $('.main').css('margin-top', bannerHeight + 'px');
  $('.top-of-page').css('margin-top', '-' + bannerHeight + 'px');
}

function doRelatedPosts() {
  let settings = {
    dots: false,
    arrows: false,
    infinite: true,
    mobileFirst: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    speed: 2000,
    autoplaySpeed: 4000,
  };

  $('.image-slider').slick(settings);
}
