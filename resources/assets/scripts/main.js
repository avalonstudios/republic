// import external dependencies
import 'jquery';

// Import everything from autoload
import './autoload/**/*'

// import local dependencies
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';
import aboutUs from './routes/about';
import singleEvent_cpt from './routes/singleEvent_cpt';
import single from './routes/single';

import 'addevent';

// Import Slick
import 'slick-carousel/slick/slick.min';

/** Populate Router instance with DOM routes */
const routes = new Router({
  // All pages
  common,
  // Home page
  home,
  // About Us page, note the change from about-us to aboutUs.
  aboutUs,
  singleEvent_cpt,
  single,
});

// Load Events
jQuery(document).ready(() => routes.loadEvents());
