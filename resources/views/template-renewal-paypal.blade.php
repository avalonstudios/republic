{{--
  Template Name: Renewal PayPal
--}}

@php
$renewalOpts = get_fields( 'options' )[ 'renewals' ];
$type = $_GET[ 'type' ];

if ($type === 'regular' || $type === 'student') {
  $lang = 'renewal_text_' . pll_current_language();
  $text = wp_kses_post( $renewalOpts[ 'renewal_text' ][ $lang ] );
} else {
  $fallbackLang = 'fallback_text_' . pll_current_language();
  $text = wp_kses_post( $renewalOpts[ 'fallback_text' ][ $fallbackLang ] );
}


@endphp

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.content-page')
    {!! $text !!}
    @include('partials.paypal-btn-code')
  @endwhile
@endsection
