{{--
  Title: Statute Block
  Description: Display Statute
  Category: ava_block_category
  Icon: admin-comments
  Keywords: statute
  Mode: edit
  Align: full
  PostTypes: page post
  SupportsAlign: true
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
$flds  = get_fields(  );
$active = $flds[ 'active' ];

if ( ! $active ) {
  return;
}

$other_classes = '';
$backImg = '';

$sectionTitle = $flds[ 'block_title' ];

$componentVars = [
  'id'              => $block[ 'id' ],
  'classes'         => $block[ 'classes' ],
  'slug'            => $block[ 'slug' ],
  'other_classes'   => " {$other_classes}",
  'title'           => $sectionTitle,
  'blockID'         => $block[ 'id' ],
  'secProps'        => $flds[ 'styles' ],
  'backImg'         => ''
];

$statute = $flds[ 'article' ];
@endphp

@component( 'components.blocks', $componentVars )
  @foreach ($statute as $article)
    @php
    $articleTitle   = $article[ 'article_title' ];
    $headings       = $article[ 'headings' ];
    @endphp
    <div>
      <ol class="article-statute">
        <h4>{{ pll__( 'Article No.' ) }} {{ $loop->iteration }} - {{ $articleTitle }}</h4>
        @foreach ($headings as $heading)
          @php
          $title        = $heading[ 'title' ];
          $header       = $heading[ 'heading' ];
          $numStyle     = $heading[ 'num_style' ];
          $subHeadings  = $heading[ 'subheadings' ];
          $headingCont  = $heading[ 'heading_contd' ];
          @endphp
          @if ( $title )
            <div class="article-name">{!! $title !!}</div>
          @endif
          <li class="article-headings">
            <div class="article-text">{!! $header !!}</div>
          </li>
          @if ( $subHeadings )
            <ol type="{{ $numStyle == 'letter' ? 'a' : 'i' }}">
              @foreach ($subHeadings as $subHeading)
              @php
              $title      = $subHeading[ 'title' ];
              $subheading = $subHeading[ 'subheading' ];
              @endphp
              <li class="subheading">{!! $subheading !!}</li>
              @endforeach
            </ol>
          @endif
          @if ( $headingCont )
            <div class="heading-continued">{!! $headingCont !!}</div>
          @endif
        @endforeach
      </ol>
    </div>
  @endforeach
  {{-- <pre>@dump($statute)</pre> --}}
@endcomponent
