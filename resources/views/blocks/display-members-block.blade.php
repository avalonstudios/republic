{{--
  Title: Display Members
  Description: Display Members drag&drop order
  Category: ava_block_category
  Icon: admin-comments
  Keywords: members committee
  Mode: edit
  Align: full
  PostTypes: page post
  SupportsAlign: true
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
  $flds  = get_fields(  );
  $active = $flds[ 'active' ];

  if ( ! $active ) {
    return;
  }

  $other_classes = '';
  $backImg = '';

  $sectionTitle = $flds[ 'block_title' ];

  $componentVars = [
    'id'              => $block[ 'id' ],
    'classes'         => $block[ 'classes' ],
    'slug'            => $block[ 'slug' ],
    'other_classes'   => " {$other_classes}",
    'title'           => $sectionTitle,
    'blockID'         => $block[ 'id' ],
    'secProps'        => $flds[ 'styles' ],
    'backImg'         => ''
  ];

  $members = $flds['select_members'];
@endphp

@component( 'components.blocks', $componentVars )
  <div class="committee-members">
    @foreach ($members as $member)

      @php
        $flds = get_fields($member->ID);

        $title = $member->post_title;
        $designation    = $flds[ 'designation' ];
        $bio            = wp_kses_post( $flds[ 'biography' ] );
        $socialIcons    = $flds[ 'social_icons' ][ 'social_icons' ];
        $image[ 'url' ] = $flds[ 'image' ];
        $image[ 'alt' ] = $title . ' - ' . $designation;
      @endphp

      <div class="single-committee-member">
        <div class="name-designation">
          <h3 class="name">{!! $title !!}</h3>
          <h4 class="designation">{{ $designation }}</h4>
        </div>
        @if($image['url'])
          <div class="image">{{ imgResize( $image, 300, 300, true, true, true ) }}</div>
        @endif
        <div class="biography">{!! $bio  !!}</div>
        @include(
          'menus.social-menu',
          [
            'classes' => ' social-committee',
            'socialIcons' => $socialIcons
          ]
        )
      </div>
      <div class="sep thin"></div>


      {{--@php
        $fields = get_fields($member->ID);

        $title = wp_kses_post($member->post_title);
        $designation = wp_kses_post($fields['designation']);
        $image = '';//aq_resize($fields['image'], 300, 300, true, true, true);
        $bio = wp_trim_words($fields['biography'], 30);
        $socials = $fields['social_icons'];
        $link = get_the_permalink($member->ID);
      @endphp


      <div class="committee-member {{ $loop->index % 2 === 0 ? 'even' : 'odd' }}">

        <div class="member-image">
          @if($image)
            <img src="{{ $image }}" alt="{!! $title !!}">
          @endif
        </div>

        <div class="member-title">
          <h4>{!! $title !!}</h4>

          @if ($designation)
            <span> - {!! $designation !!}</span>
          @endif
        </div>

        <div class="member-bio">{!! $bio !!}</div>

        @if($bio)
          <a href="{{ $link }}" rel="bookmark" class="member-link"></a>
        @endif
      </div>--}}

    @endforeach
  </div>
  {{-- <pre>@dump($flds)</pre> --}}
@endcomponent
