{{--
  Title: Bank Details
  Description: Display bank details
  Category: ava_block_category
  Icon: admin-comments
  Keywords: bank, details
  Mode: edit
  Align: full
  PostTypes: page post
  SupportsAlign: true
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
$flds  = get_fields(  );
$active = $flds[ 'active' ];

if ( ! $active ) {
  return;
}

$other_classes = '';
$backImg = '';

$sectionTitle = $flds[ 'block_title' ];

$componentVars = [
  'id'              => $block[ 'id' ],
  'classes'         => $block[ 'classes' ],
  'slug'            => $block[ 'slug' ],
  'other_classes'   => " {$other_classes}",
  'title'           => $sectionTitle,
  'blockID'         => $block[ 'id' ],
  'secProps'        => $flds[ 'styles' ],
  'backImg'         => ''
];

$banks = $flds[ 'banks' ];
@endphp

@component( 'components.blocks', $componentVars )
  @foreach ($banks as $k => $bank)
    <div class="bank-detail-wrapper">
      <div class="bank-detail-name">{{ pll__( 'Bank Name' ) }}</div>
      <div class="copy-element bank_name">{{ $bank[ 'bank_name' ] }}</div>
      <div id="bank_name-{{ $loop->iteration }}" class="hide-element">{{ $bank[ 'bank_name' ] }}</div>
      <span class="copy-btn" data-clipboard-target="#bank_name-{{ $loop->iteration }}">{{ pll__( 'copy' ) }}</span>
      <div class="tooltip-text">{{ pll__( 'Copied' ) }}: <strong>{{ $bank[ 'bank_name' ] }}</strong></div>
    </div>
    <div class="bank-detail-wrapper">
      <div class="bank-detail-name">{{ pll__( 'Account Name' ) }}</div>
      <div class="copy-element account_name">{{ $bank[ 'account_name' ] }}</div>
      <div id="account_name-{{ $loop->iteration }}" class="hide-element">{{ $bank[ 'account_name' ] }}</div>
      <span class="copy-btn" data-clipboard-target="#account_name-{{ $loop->iteration }}">{{ pll__( 'copy' ) }}</span>
      <div class="tooltip-text">{{ pll__( 'Copied' ) }}: <strong>{{ $bank[ 'account_name' ] }}</strong></div>
    </div>
    <div class="bank-detail-wrapper">
      <div class="bank-detail-name">{{ pll__( 'Account Number' ) }}</div>
      <div class="copy-element account_number">{{ $bank[ 'account_number' ] }}</div>
      <div id="account_number-{{ $loop->iteration }}" class="hide-element">{{ alphaNumbericOnly( $bank[ 'account_number' ] ) }}</div>
      <span class="copy-btn" data-clipboard-target="#account_number-{{ $loop->iteration }}">{{ pll__( 'copy' ) }}</span>
      <div class="tooltip-text">{{ pll__( 'Copied' ) }}: <strong>{{ $bank[ 'account_number' ] }}</strong></div>
    </div>
    <div class="bank-detail-wrapper">
      <div class="bank-detail-name">{{ pll__( 'IBAN' ) }}</div>
      <div class="copy-element iban">{{ $bank[ 'iban' ] }}</div>
      <div id="iban-{{ $loop->iteration }}" class="hide-element">{{ alphaNumbericOnly( $bank[ 'iban' ] ) }}</div>
      <span class="copy-btn" data-clipboard-target="#iban-{{ $loop->iteration }}">{{ pll__( 'copy' ) }}</span>
      <div class="tooltip-text">{{ pll__( 'Copied' ) }}: <strong>{{ $bank[ 'iban' ] }}</strong></div>
    </div>
  @endforeach
@endcomponent
