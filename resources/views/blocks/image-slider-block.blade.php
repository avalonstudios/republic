{{--
  Title: Image Slider
  Description: Show image slider
  Category: ava_block_category
  Icon: admin-comments
  Keywords:
  Mode: edit
  Align: full
  PostTypes: page post
  SupportsAlign: true
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
$flds  = get_fields(  );
$active = $flds[ 'active' ];

if ( ! $active ) {
  return;
}

$slides = $flds[ 'slides' ];

if ( ! $slides ) {
  return;
}

$other_classes = 'full-width top-of-page';
$backImg = '';

$sectionTitle = $flds[ 'block_title' ];

$componentVars = [
  'id'              => $block[ 'id' ],
  'classes'         => $block[ 'classes' ],
  'slug'            => $block[ 'slug' ],
  'other_classes'   => " {$other_classes}",
  'title'           => $sectionTitle,
  'blockID'         => $block[ 'id' ],
  'secProps'        => $flds[ 'styles' ],
  'backImg'         => ''
];
@endphp

@component( 'components.blocks', $componentVars )
  <div class="image-slider">
    @foreach ($slides as $slide)
      @php
        $image  = $slide[ 'image' ];
        $title  = $slide[ 'title' ];
        $text   = $slide[ 'text' ];
        $button   = $slide[ 'link' ];
      @endphp
      <div class="image-slide">
        {{ imgResize( $image, 1920, 1080, true, true, true ) }}
        <div class="slide-content-wrapper">
          @if ( $title or $text or $button )
          <div class="slide-content">
            <h2 class="image-title">{{ $title }}</h2>
            <h3 class="image-text">{{ $text }}</h3>
            @include('components.btn')
          </div>
          @endif
        </div>
      </div>
    @endforeach
  </div>
  {{-- <pre>@dump($flds)</pre> --}}
@endcomponent
