{{--
  Title: Autoplay Video
  Description: Display a video which will start playing as soon as the page loads
  Category: ava_block_category
  Icon: admin-comments
  Keywords: autoplay, video
  Mode: edit
  Align: full
  PostTypes: page post
  SupportsAlign: true
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
$flds  = get_fields(  );
$active = $flds[ 'active' ];

if ( ! $active ) {
  return;
}

$other_classes = '';
$backImg = '';

$sectionTitle = $flds[ 'block_title' ];

$componentVars = [
  'id'              => $block[ 'id' ],
  'classes'         => $block[ 'classes' ],
  'slug'            => $block[ 'slug' ],
  'other_classes'   => " {$other_classes}",
  'title'           => $sectionTitle,
  'blockID'         => $block[ 'id' ],
  'secProps'        => $flds[ 'styles' ],
  'backImg'         => ''
];
$video = $flds[ 'video_embed' ];
@endphp

@component( 'components.blocks', $componentVars )
  <div class="embed-container">
    <iframe
      {{-- width="560" --}}
      {{-- height="315" --}}
      src="https://www.youtube.com/embed/{{ $video }}?rel=0;&autoplay=1&mute=1&loop=1&playlist={{ $video }}"
      frameborder="0"
      allowfullscreen
      include
    ></iframe>
  </div>
@endcomponent
