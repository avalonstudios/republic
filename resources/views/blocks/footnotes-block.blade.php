{{--
  Title: Footnotes
  Description: Block_Description
  Category: ava_block_category
  Icon: admin-comments
  Keywords: keywords
  Mode: edit
  Align: full
  PostTypes: page post publication_cpt
  SupportsAlign: true
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
$flds  = get_fields(  );
$active = $flds[ 'active' ];

if ( ! $active ) {
  return;
}

$other_classes = '';
$backImg = '';

$sectionTitle = $flds[ 'block_title' ];

$componentVars = [
  'id'              => $block[ 'id' ],
  'classes'         => $block[ 'classes' ],
  'slug'            => $block[ 'slug' ],
  'other_classes'   => " {$other_classes}",
  'title'           => $sectionTitle,
  'blockID'         => $block[ 'id' ],
  'secProps'        => $flds[ 'styles' ],
  'backImg'         => ''
];

$footnotes = $flds[ 'footnotes' ];
@endphp

@component( 'components.blocks', $componentVars )
  <div class="footnotes">
    @foreach ($footnotes as $footnote)
      @php
      $note = wp_kses_post($footnote[ 'note' ]);
      @endphp
      <div class="footnote">
        <sup class="fn-number">{{ $footnote[ 'number' ] }}</sup>
        <div class="fn-text">{!! $note !!}</div>
      </div>
    @endforeach
</div>
@endcomponent
