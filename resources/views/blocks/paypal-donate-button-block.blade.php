{{--
  Title: PayPal Donate Button
  Description: Display the donate button
  Category: ava_block_category
  Icon: admin-comments
  Keywords: paypal, donate, donations
  Mode: edit
  Align: full
  PostTypes: page post
  SupportsAlign: true
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
$flds  = get_fields(  );
$active = $flds[ 'active' ];

if ( ! $active ) {
  return;
}

$other_classes = '';
$backImg = '';

$sectionTitle = $flds[ 'block_title' ];

$componentVars = [
  'id'              => $block[ 'id' ],
  'classes'         => $block[ 'classes' ],
  'slug'            => $block[ 'slug' ],
  'other_classes'   => " {$other_classes}",
  'title'           => $sectionTitle,
  'blockID'         => $block[ 'id' ],
  'secProps'        => $flds[ 'styles' ],
  'backImg'         => ''
];

$btnCode = $flds[ 'paypal_button_code' ];
$qrCode = $flds[ 'qr_code' ];
@endphp

@component( 'components.blocks', $componentVars )
  <div class="paypal-donation-button">{!! $btnCode !!}</div>
  <div class="paypal-donation-qrcode"><img src="{{ $qrCode }}" alt="Repubblika Paypal QR Code"></div>
@endcomponent
