{{--
  Title: Motto
  Description: Display text in large font
  Category: ava_block_category
  Icon: admin-comments
  Keywords: motto, large, font
  Mode: edit
  Align: full
  PostTypes: page post
  SupportsAlign: true
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
$flds  = get_fields(  );
$active = $flds[ 'active' ];

if ( ! $active ) {
  return;
}

$other_classes = '';
$backImg = '';

$sectionTitle = $flds[ 'block_title' ];

$componentVars = [
  'id'              => $block[ 'id' ],
  'classes'         => $block[ 'classes' ],
  'slug'            => $block[ 'slug' ],
  'other_classes'   => " {$other_classes}",
  'title'           => $sectionTitle,
  'blockID'         => $block[ 'id' ],
  'secProps'        => $flds[ 'styles' ],
  'backImg'         => ''
];

$text = wp_kses_post( $flds[ 'text' ] );
$smallText = wp_kses_post( $flds[ 'small_text' ] );
@endphp

@component( 'components.blocks', $componentVars )
  <div class="text-wrapper">
    <div class="motto-text">{!! $text !!}</div>
    <div class="small-text">{!! $smallText !!}</div>
  </div>
  {{-- <a href="#next-screen" class="scroll-next-screen"><i class="fas fa-chevron-circle-down"></i></a>
  <div id="next-screen"></div> --}}
@endcomponent
