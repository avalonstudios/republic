{{--
  Title: File Download
  Description: Upload a file for users to view or download
  Category: ava_block_category
  Icon: admin-comments
  Keywords: file, upload, download
  Mode: edit
  Align: full
  PostTypes: page post pressrelease_cpt
  SupportsAlign: true
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
$flds  = get_fields(  );
$active = $flds[ 'active' ];

if ( ! $active ) {
  return;
}

$other_classes = '';
$backImg = '';

$sectionTitle = $flds[ 'block_title' ];

$componentVars = [
  'id'              => $block[ 'id' ],
  'classes'         => $block[ 'classes' ],
  'slug'            => $block[ 'slug' ],
  'other_classes'   => " {$other_classes}",
  'title'           => $sectionTitle,
  'blockID'         => $block[ 'id' ],
  'secProps'        => $flds[ 'styles' ],
  'backImg'         => ''
];
@endphp

@component( 'components.blocks', $componentVars )
  @include('partials.press-releases.file-download')
@endcomponent
