@php
if ( is_post_type_archive( 'pressrelease_cpt' ) || is_singular( 'pressrelease_cpt' ) ) {
  return;
}

$widgetTitle = $instance[ 'title' ];
$numOfPosts = $flds[ 'num_to_display' ];
$latestPRs = Widget_Latest_Press_Releases::latestPressReleases( $numOfPosts );
@endphp

{!! $args[ 'before_widget' ] !!}
@if ( $widgetTitle )
{!! $args[ 'before_title' ] !!}{!!
$widgetTitle
!!}{!! $args[ 'after_title' ] !!}
@endif

@foreach ( $latestPRs as $latest )
  <div class="widget-press-release">
    <div class="pr-title">
      <a href="{{ $latest[ 'link' ] }}" rel="bookmark">
        <h3>{!! $latest[ 'title' ] !!}</h3>
      </a>
      @include('partials.entry-meta', [ 'prID' => $latest[ 'id' ], 'classes' => 'widget-entry-meta' ])
    </div>
    <div class="pr-image">
      <a href="{{ $latest[ 'link' ] }}" rel="bookmark">
        <img src="{{ $latest[ 'thumb' ] }}" alt="{!! $latest[ 'title' ] !!}">
      </a>
    </div>
    <div class="entry-content">{{ $latest[ 'excerpt' ] }}</div>
    @include(
      'components.btn',
      [
        $button[ 'url' ] = $latest[ 'link' ],
        $button[ 'title' ] = 'read more',
        $button[ 'target' ] = '',
      ]
    )
  </div>
@endforeach

{!! $args[ 'after_widget' ] !!}
