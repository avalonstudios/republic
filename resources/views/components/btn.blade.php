@php
if ( ! isset( $button[ 'url' ] ) ) {
  return;
}

$url      = $button[ 'url' ];
$target   = $button[ 'target' ];
$title    = $button[ 'title' ];


if ( isset( $button[ 'type' ] ) ) {
  $type = $button[ 'type' ];
}

if ( isset( $button[ 'attr' ] ) ) {
  $attr    = $button[ 'attr' ];
} else {
  $attr    = '';
}
@endphp

<div class="btn-wrap{{ $wrapClass ?? '' }} wrap-{{ $type ?? 'primary' }}">
  <a
    href="{{ $url }}" {{ $attr }}
    class="btn btn-{{ $type ?? 'primary' }}{{ $classes ?? '' }}"
    {{ $target ? ' target="_blank" rel="noopener nofollow"' : ' rel="bookmark"' }}
  >{!! pll__( $title ) ?? pll__( 'read more' ) !!}</a>
</div>
