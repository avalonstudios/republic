<li class="nav-item">
  <a
    class="nav-link waves-effect"
    href="{{ $sIcon[ 'link' ] }}"
    target="_blank"
    rel="noopener noreferrer"
    title="{{ $sIcon[ 'social_icon' ][ 'label' ] }}"
  >
    <i class="fab {{ $sIcon[ 'social_icon' ][ 'value' ] }}"></i>
  </a>
</li>
