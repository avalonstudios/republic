{{--
  Template Name: Committee Members Page
--}}

@extends('layouts.app')

@section('content')
<div class="committee-members-list">
    @foreach ($members as $member)
    @php
    $name = $member->post_title;
    $memberID = $member->ID;
    $link = get_the_permalink( $memberID );
    $flds = get_fields( $memberID );
    $designation = $flds[ 'designation' ];
    $bio = $flds[ 'biography' ];
    $socialIcons = $flds[ 'social_icons' ][ 'social_icons' ];
    $image[ 'url' ] = $flds[ 'image' ];
    $image[ 'alt' ] = $name . ' - ' . $designation;
    @endphp

    <div class="committee-member">
        @if ($image['url'])
            @if ( $bio )
                <a href="{{ $link }}"
                rel="bookmark">
                    <div class="committee-image">{{ imgResize( $image, 300, 300, true, true, true ) }}</div>
                </a>
            @else
                <div class="committee-image">{{ imgResize( $image, 300, 300, true, true, true ) }}</div>
            @endif
        @endif

        @if ( $bio )
            <h4 class="committee-name"><a href="{{ $link }}"
                rel="bookmark">{{ $name }}</a></h4>
        @else
            <h4 class="committee-name">{{ $name }}</h4>
        @endif

        @if ($designation)
            <div class="committee-designation">{{ $designation }}</div>
        @endif

        @include(
            'menus.social-menu',
            [
                'classes' => ' social-committee',
                'socialIcons' => $socialIcons
            ]
        )
    </div>
    @endforeach
</div>
@endsection