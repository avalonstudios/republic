@php
if ( ! $socialIcons ) {
  return;
}
@endphp

<div class="social-icons-menu{{ $classes }}">
  <ul>
    @each( 'components.social-icon-li-a', $socialIcons, 'sIcon' )
  </ul>
</div>
