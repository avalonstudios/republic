<nav class="nav-primary hide">
  @if (has_nav_menu('primary_navigation'))
    {!! wp_nav_menu(
      [
        'theme_location' => 'primary_navigation',
        'container_class' => 'main-menu',
        'menu_class' => 'nav',
        'echo' => false,
        'walker' => new MDBootstrapMenu()
      ]
    ) !!}
  @endif
</nav>
