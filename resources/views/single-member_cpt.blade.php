@php
$flds = get_fields();
$designation    = $flds[ 'designation' ];
$bio            = wp_kses_post( $flds[ 'biography' ] );
$socialIcons    = $flds[ 'social_icons' ][ 'social_icons' ];
$image[ 'url' ] = $flds[ 'image' ];
$image[ 'alt' ] = get_the_title() . ' - ' . $designation;
@endphp

@extends('layouts.app')

@section('content')
  <div class="single-committee-member">
    <div class="name-designation">
      <h1 class="name">{!! the_title() !!}</h1>
      <h4 class="designation">{{ $designation }}</h4>
    </div>
    <div class="image">{{ imgResize( $image, 300, 300, true, true, true ) }}</div>
    <div class="biography">{!! $bio  !!}</div>
    <div class="sep thin"></div>
    @include(
      'menus.social-menu',
      [
        'classes' => ' social-committee',
        'socialIcons' => $socialIcons
      ]
    )
  </div>
@endsection
