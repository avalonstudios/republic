@if ( $get_announcements )
  <div id="announcements">
  <div class="announcements-toggle">{{ pll__( 'Announcements' ) }}</div>
    <div class="announcements-wrapper hide">
      <div class="announcements-slider">
        @foreach ($get_announcements as $ann)
          <div class="announcement">
            <h4 class="announcement-title">{!! $ann[ 'title' ] !!}</h4>
            {!! $ann[ 'content' ] !!}
          </div>
        @endforeach
      </div>
    </div>
  </div>
@endif



{{-- @if ( $get_announcements )
<div id="announcements">
  <div class="announcements-toggle">{{ pll__( 'Announcements' ) }}</div>
  <div class="announcements-wrapper hide">
    <div class="announcements-slider">
      @foreach ($get_announcements as $ann)
      <div class="announcement">
        <h4 class="announcement-title">{!! $ann[ 'title' ] !!}</h4>
        {!! $ann[ 'announcement' ] !!}
      </div>
      @endforeach
    </div>
  </div>
</div>
@endif --}}
