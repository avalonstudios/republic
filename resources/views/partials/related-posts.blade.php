@if ( $get_related_posts )
  <h2 class="related-posts-title">{{ pll__( 'Related Posts' ) }}</h2>
  <div id="related-posts">
    @foreach ( $get_related_posts as $rp )
      <div class="related-post">
        @if ( $rp[ 'image' ] )
          <a href="{{ $rp[ 'link' ] }}" rel="bookmark">
            <img src="{{ $rp[ 'image' ] }}" alt="{{ $rp[ 'title' ] }}">
          </a>
        @else
          <a href="{{ $rp[ 'link' ] }}" rel="bookmark">
            <img src="@asset('images/no-image.png')" alt="{{ $rp[ 'title' ] }}">
          </a>
        @endif
        <a href="{{ $rp[ 'link' ] }}" rel="bookmark">{{ $rp[ 'title' ] }}</a>
        <time class="updated" datetime="{{ date( 'c', $rp[ 'date' ] ) }}">{{ date( 'd/m/Y', $rp[ 'date' ] ) }}</time>

        @if ( $rp[ 'modified_date' ] > $rp[ 'date' ] )
          <span class="updated-text">{{ pll__( 'updated' ) }}</span>
          <time class="modified" datetime="{{ date( 'c', $rp[ 'modified_date' ] ) }}">{{ date( 'd/m/Y H:i', $rp[ 'modified_date' ] ) }}</time>
        @endif
      </div>
    @endforeach
  </div>
@endif
