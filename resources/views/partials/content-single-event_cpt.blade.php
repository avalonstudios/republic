@php
$flds = get_fields();
/* if ( ! isset( $event ) ) {
  $flds = get_fields();
} else {
  $flds = get_fields( $event );
} */
$getVenue = $flds[ 'venue' ];
$map = get_fields( $getVenue )[ 'map' ];
@endphp

<article @php post_class() @endphp>
  <div class="entry-content">
    @include(
      'partials.events.event-details',
      [
        'displaySingleDates' => false,
        'displayMultipleDates' => true
      ]
    )
  </div>
  @if ( is_singular( 'event_cpt' ) )
    @include('partials.events.event-map', [ 'map' => $map ])
  @endif
</article>
