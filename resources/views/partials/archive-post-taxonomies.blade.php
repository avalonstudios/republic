@php
$postCategories = App::getThePostTaxonomies()[ 'categories' ];
$postTags = App::getThePostTaxonomies()[ 'post_tag' ];

if ( $postCategories ) {
  $postCategories = collect( $postCategories )->slice(0, 3);
}

if ( $postTags ) {
  $postTags = collect( $postTags )->slice(0, 3);
}
@endphp

@if ( isset( $postCategories ) || isset( $postTags ) )
  <div class="archive-post-categories-tags">
    @if ( $postCategories )
      <div class="post-categories">
        <i class="fas fa-folder-open"></i>
        <div class="taxonomy-links">
          @foreach ( $postCategories as $taxes )
            @if ( ! $loop->last )
              <a href="{{ $taxes[ 'link' ] }}" rel="bookmark">{{ $taxes[ 'name' ] }}</a>,
            @else
              <a href="{{ $taxes[ 'link' ] }}" rel="bookmark">{{ $taxes[ 'name' ] }}</a>
            @endif
          @endforeach
        </div>
      </div>
    @endif

    @if ( $postTags )
      <div class="post-tags">
        <i class="fas fa-tag"></i>
        <div class="taxonomy-links">
          @foreach ( $postTags as $taxes )
            @if ( ! $loop->last )
              <a href="{{ $taxes[ 'link' ] }}" rel="bookmark">{{ $taxes[ 'name' ] }}</a>,
            @else
              <a href="{{ $taxes[ 'link' ] }}" rel="bookmark">{{ $taxes[ 'name' ] }}</a>
            @endif
          @endforeach
        </div>
      </div>
    @endif
  </div>
@endif
