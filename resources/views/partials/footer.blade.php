<a href="#top" class="btn btn-circle" id="back-to-top"><i class="fas fa-angle-up"></i></a>
<footer class="content-info">
  <div class="container">
    @php dynamic_sidebar('sidebar-footer') @endphp
  </div>

  <div class="copyright-design">
    <div class="copyright"><a href="{{ get_bloginfo( 'url' ) }}" rel="home">Copyright &copy; {{ date( 'Y' ) }} {{ get_bloginfo( 'name' ) }}</a></div>
    <div class="sep">|</div>
    <div class="design"><a href="https://avalonstudios.eu" target="_blank" rel="nofollow">Design by Avalon Studios</a>
    </div>
    <div class="sep">|</div>
    <div class="powered"><a href="https://cloud1500.com" target="_blank" rel="nofollow">Powered by Cloud1500</a></div>
  </div>
</footer>

{{-- <div class="footer-menu">
  <div class="close-menu"><i class="fas fa-times"></i></div>
  @include(
    'menus.social-menu',
    [
      'classes' => ' social-footer hide',
      'socialIcons' => App::getOptions()[ 'social_icons' ][ 'social_icons' ]
    ]
  )
</div> --}}
