{{-- from App/Single --}}
@if ( $get_the_next_post && is_single() )
<div class="prev-next-post post-next">
  <a href="{{ $get_the_next_post[ 'link' ] }}" rel="bookmark">
    <h2>{{ $get_the_next_post[ 'title' ] }}</h2>
    <img src="{{ $get_the_next_post[ 'thumb' ] }}" alt="{{ $get_the_next_post[ 'title' ] }}">
  </a>
</div>
@endif
