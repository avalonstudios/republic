<article @php post_class() @endphp>
  @include('partials.get-post-terms', [ 'postType' => 'video_type' ])
  <div class="entry-content">
    @php the_content() @endphp
    @include('partials.video.facebook-embed')
    {{-- <div class="embed-container">{!! $video !!}</div> --}}
  </div>
  @php comments_template('/partials/comments.blade.php') @endphp
</article>
