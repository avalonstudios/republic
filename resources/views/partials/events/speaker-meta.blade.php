@if ( $speakers )
  <div id="speakers">
    @foreach ($speakers as $speaker)
      <div class="speaker-meta">
        @php
        $name = get_the_title( $speaker );
        $flds = get_fields( $speaker );
        $showEmail = $flds[ 'show_email' ];
        $showNum = $flds[ 'show_contact_number' ];
        $email = $flds[ 'email' ];
        $number = $flds[ 'contact_number' ];
        $website = $flds[ 'artist_website' ];
        $image[ 'url' ] = $flds[ 'image' ];
        $image[ 'alt' ] = $name;
        $bio = wp_kses_post( wpautop( $flds[ 'short_bio' ] ) );
        @endphp

        <h4>{{ $name }}</h4>

        @if ( $image[ 'url' ] )
        <div class="image">{{ imgResize( $image, 150, 150, true, true, true ) }}</div>
        @endif

        @if ( $showEmail or $showNum or $website )
        <div class="speaker-details">
          @if ( $showEmail )
          <div class="speaker-detail email">
            <span class="detail-title">e:</span>
            <a href="mailto:{{ $email }}">{{ $email }}</a>
          </div>
          @endif

          @if ( $showNum )
          <div class="speaker-detail number">
            <span class="detail-title">t:</span>
            <a href="tel:+{{ $number[ 'cc' ] }}{{ $number[ 'number' ] }}">+{{ $number[ 'cc' ] }}
              {{ $number[ 'number' ] }}</a>
          </div>
          @endif

          @if ( $website )
          <div class="speaker-detail website">
            <span class="detail-title">w:</span>
            <a href="{{ $website }}" rel="noopener noreferrer" target="_blank">website</a>
          </div>
          @endif
        </div>
        @endif

        @if ( $bio )
        <div class="bio">{!! $bio !!}</div>
        @endif
      </div>
    @endforeach
  </div>
@endif
