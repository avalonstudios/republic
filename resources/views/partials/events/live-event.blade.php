@php
$msg = SingleEventCpt::getMsg();

if ( ! $msg[ 'live_event' ] ) {
  return;
}
@endphp

<div class="live-event-msg">{!! $msg[ 'msg' ] !!}</div>
