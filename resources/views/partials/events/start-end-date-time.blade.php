<div class="start-date-time">
  <span class="start-date">
    @if ( pll_current_language() === 'mt' )
      <i class="far fa-calendar-alt"></i> {{ pll__( date('D', $startDate) ) }}, {{ date('d', $startDate) }} ta' {{ pll__( date('F', $startDate) ) }} {{ date('Y', $startDate) }}
    @else
      <i class="far fa-calendar-alt"></i> {{ date('D, dS M Y', $startDate) }}
    @endif
  </span>
  <span class="start-time">
    <i class="far fa-clock"></i> {{ date('H:i', $startDate) }}
  </span>
</div>
<div class="end-date-time">
  <span class="end-date">
    @if ( pll_current_language() === 'mt' )
      <i class="far fa-calendar-alt"></i> {{ pll__( date('D', $endDate) ) }}, {{ date('d', $endDate) }} ta' {{ pll__( date('F', $endDate) ) }} {{ date('Y', $endDate) }}
    @else
      <i class="far fa-calendar-alt"></i> {{ date('D, dS M Y', $endDate) }}
    @endif
  </span>
  <span class="end-time">
    <i class="far fa-clock"></i> {{ date('H:i', $endDate) }}
  </span>
</div>
