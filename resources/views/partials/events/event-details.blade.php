@php
if ( ! isset( $addID ) ) {
  $flds = get_fields();
} else {
  $flds = get_fields( $addID );
}

if ( ! isset( $addID ) ) {
  $addID = get_the_ID();
}

$startDate      = $flds[ 'start_date' ];
$endDate        = $flds[ 'end_date' ];
$singleDates    = $flds[ 'select_individual_dates' ];

$speakers       = $flds[ 'speaker' ];

$venue          = $flds[ 'venue' ];
$venueFlds      = get_fields( $venue );

$admittance     = $flds[ 'admittance' ];
$price          = $flds[ 'price' ];
$ticketURL      = $flds[ 'ticket_url' ];
$contactNumber  = $flds[ 'contact_number' ];
$notes          = wpautop( wp_kses_post( $flds[ 'notes' ] ) );

// set the proper permalink
if ( get_the_ID() === $addID ) {
  $link = get_the_permalink();
} else {
  $link = get_the_permalink( $addID );
}

@endphp

@include('partials.events.event-meta')

@include('partials.events.venue-meta')

@if ( is_singular( 'event_cpt' ) )
<div class="biography">
  <h3>{{ pll__( 'Event Details' ) }}</h3>
  {!! $notes !!}
</div>
@include('partials.events.speaker-meta')
@endif

@if ( !is_singular( 'event_cpt' ) )
@include(
  'components.btn',
  [
    $button[ 'url' ] = $link,
    $button[ 'title' ] = 'Details',
    $button[ 'target' ] = '',
  ]
)
@endif

@include('partials.events.addevent')

{{-- @include('partials.events.event-map') --}}

@if ( is_singular( 'event_cpt' ) )
@include('partials.events.live-event')
@endif
