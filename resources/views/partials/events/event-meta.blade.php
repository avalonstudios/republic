<div class="event-meta">
  @if ( $startDate && $endDate )
  @include('partials.events.start-end-date-time')
  @endif

  @if ( $singleDates && $displayMultipleDates )
    <div class="multiple-dates-times">
      @foreach ($singleDates as $singleDate)
        @php
        $startDate  = $singleDate[ 'start_date' ];
        $endDate    = $singleDate[ 'end_date' ];
        @endphp
        @include('partials.events.start-end-date-time')
      @endforeach
    </div>
  @endif

  <h4>{{ pll__( 'Admittance' ) }}</h4>

  <div class="admittance-level">{{ pll__( $admittance ) }}</div>

  @if ( $price )
  <div class="admittance-price">&euro;{{ $price }}</div>
  @endif
</div>

<div class="event-links">
  @if ( $ticketURL || $contactNumber[ 'number' ] )
  <h4>{{ pll__( 'Tickets' ) }}</h4>
  @endif

  @if ( $ticketURL )
  <div class="ticket-url">
    <a href="{{ $ticketURL }}" rel="noopener noreferrer" target="_blank">{{ pll__( 'website' ) }}</a>
  </div>
  @endif

  @if ( $contactNumber[ 'number' ] )
  <div class="ticket-phone">
    <a href="tel:+{{ $contactNumber[ 'cc' ] }}{{ $contactNumber[ 'number' ] }}">{{ pll__( 'phone' ) }}</a>
  </div>
  @endif
</div>
