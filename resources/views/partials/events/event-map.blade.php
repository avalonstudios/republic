@php
if ( !$map[ 'lat' ] ) {
  return;
}

if ( !$map[ 'icon' ] ) {
  $icon = get_stylesheet_directory_uri() . '/assets/images/map-marker.png';
} else {
  $icon = $map[ 'icon' ];
}

@endphp

<div
  id="mapid"
  style="height: 400px;"
  data-lat="{{ $map[ 'lat' ] }}"
  data-lng="{{ $map[ 'lng' ] }}"
  data-icon="{{ $icon }}"
></div>
