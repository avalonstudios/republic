@php
$name       = get_the_title( $venue );
$address    = $venueFlds[ 'venue_address' ];
$address2   = $venueFlds[ 'venue_address_2' ];
$city       = $venueFlds[ 'venue_city' ];
$state      = $venueFlds[ 'venue_state_provence' ];
$postalCode = $venueFlds[ 'venue_postal_code' ];
$country    = $venueFlds[ 'venue_country' ];
$website    = $venueFlds[ 'venue_website' ];
$number     = $venueFlds[ 'contact_number' ];
$map        = $venueFlds[ 'map' ];
@endphp

<div class="venue-meta">
  <h3><i class="fas fa-map-pin pr-1"></i> {!! pll__( $name ) !!}</h3>
  <address>
    <div class="address1">
      {{ $address }}
    </div>
    @if ( $address2 )
      <div class="address2">
        {{ $address2 }}
      </div>
    @endif
    <div class="city">
      {{ $city }}
    </div>
    <div class="state">
      {{ $state }}
    </div>
    @if ( $postalCode )
      <div class="post-code">
        {{ $postalCode }}
      </div>
    @endif
    @if ( $country )
      <div class="country">
        {{ $country[ 'label' ] }}
      </div>
    @endif
  </address>
</div>

<div class="venue-links">
  @if ( $website || $number[ 'number' ] )
  <h4>{{ pll__( 'Venue' ) }}</h4>
  @endif
  @if ( $website )
  <div class="website"><a href="{{ $website }}" rel="noopener noreferrer" target="_blank">{{ pll__( 'website' ) }}</a></div>
  @endif

  @if ( $number[ 'number' ] )
  <div class="contact-number"><a href="tel:+{{ $number[ 'cc' ] }}{{ $number[ 'number' ] }}">{{ pll__( 'phone' ) }}</a></div>
  @endif
</div>
