@php
$city   = $venueFlds[ 'venue_city' ];
@endphp

<!-- Button code -->
<div title="Add to Calendar" class="addeventatc">
    <div class="btn-name">{{ pll__( 'Add to Calendar' ) }}</div>
    {{-- <span class="start">06/20/2020 08:00 AM</span> --}}
    <span class="start">{{ date('d-m-Y H:i', $startDate) }}</span>
    <span class="end">{{ date('d-m-Y H:i', $endDate) }}</span>
    <span class="timezone">Europe/Rome</span>
    <span class="title">{{ the_title() }}</span>
    <span class="description">{!! $notes !!}</span>
    <span class="location">{{ $city }}</span>
</div>
