@if ( isset( $get_the_post_taxonomies[ 'categories' ] ) && isset( $get_the_post_taxonomies[ 'post_tag' ] ) )
  <div class="post-categories-tags">
    @if ( $get_the_post_taxonomies[ 'categories' ] )
      <div class="post-categories">
        <i class="fas fa-folder-open"></i>
        <span class="taxonomy-title">{{ pll__( 'Categories' ) }}</span>
        <div class="taxonomy-links">
          @foreach ( $get_the_post_taxonomies[ 'categories' ] as $taxes )
            @if ( ! $loop->last )
              <a href="{{ $taxes[ 'link' ] }}" rel="bookmark">{{ $taxes[ 'name' ] }}</a>,
            @else
              <a href="{{ $taxes[ 'link' ] }}" rel="bookmark">{{ $taxes[ 'name' ] }}</a>
            @endif
          @endforeach
        </div>
      </div>
    @endif

    @if ( $get_the_post_taxonomies[ 'post_tag' ] )
      <div class="post-tags">
        <i class="fas fa-tag"></i>
        <span class="taxonomy-title">{{ pll__( 'Tags' ) }}</span>
        <div class="taxonomy-links">
          @foreach ( $get_the_post_taxonomies[ 'post_tag' ] as $taxes )
            @if ( ! $loop->last )
              <a href="{{ $taxes[ 'link' ] }}" rel="bookmark">{{ $taxes[ 'name' ] }}</a>,
            @else
              <a href="{{ $taxes[ 'link' ] }}" rel="bookmark">{{ $taxes[ 'name' ] }}</a>
            @endif
          @endforeach
        </div>
      </div>
    @endif
  </div>
@endif
