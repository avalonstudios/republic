@php
$flds     = get_field( 'file' );
if ( $flds ) {
  $filename = $flds[ 'filename' ];
  $url      = $flds[ 'url' ];
  $icon     = $flds[ 'icon' ];
  $subtype  = strtoupper( $flds[ 'subtype' ] );
}
@endphp

<div class="file-download{{ !$flds ? ' no-download' : '' }}">
  @if ( isset( $url ) )
    <a class="view-file" href="{{ $url }}" target="_blank" rel="prerender">
      <img src="{{ $icon }}" alt="{{ $filename }}">
      <span class="view">{{ pll__( 'view' ) }}</span>
    </a>
    @include(
      'components.btn',
      [
        $button[ 'url' ]    = $url,
        $button[ 'target' ] = '',
        $button[ 'title' ]  = 'Download ' . $subtype,
        $button[ 'attr' ]   = 'download'
      ]
    )
  @endif
  @if ( is_archive() )
    @include(
      'components.btn',
      [
        $button[ 'url' ] = get_the_permalink(),
        $button[ 'title' ] = 'read more',
        $button[ 'target' ] = '',
        $button[ 'type' ] = 'secondary',
        $button[ 'attr' ] = ''
      ]
    )
  @endif
</div>
