@php
$prNo = App::getPrNos( get_the_ID() )
@endphp

<article @php post_class() @endphp>
  <header>
    <h2 class="entry-title">
      <a href="{{ the_permalink() }}" rel="bookmark">{!! get_the_title() !!}</a>
    </h2>

    @if ( $prNo )
    <div class="pr-number-container">
      <span class="pr-number">{{ pll__( 'Press Release' ) }} PR {{ $prNo }}</span>
    </div>
    @endif

    @include('partials.entry-meta')
  </header>

  <a class="post-thumbnail" href="{{ the_permalink() }}" rel="bookmark">
    <div class="image" style="background-image: url('{{ App::getThePostThumbnail( get_the_ID(), 600, 600 ) }}');"></div>
  </a>

  <div class="entry-content">
    {!! App::shorten( get_the_excerpt(), 150 ) !!}
  </div>

  <footer>
    @include('partials.press-releases.file-download')
  </footer>
</article>
