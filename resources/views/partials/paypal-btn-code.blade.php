@if ( $_GET[ 'type' ] === 'regular' )

<h3>Regular - &euro;25</h3>

@include('partials.paypal.regular')

{{-- <div id="smart-button-container">
  <div style="text-align: center">
    <div id="paypal-button-container"></div>
  </div>
</div>
<script src="https://www.paypal.com/sdk/js?client-id=sb&currency=EUR"
        data-sdk-integration-source="button-factory"></script>
<script>
  function initPayPalButton() {
    paypal
      .Buttons({
        style: {
          shape: "rect",
          color: "gold",
          layout: "vertical",
          label: "paypal",
        },

        createOrder: function (data, actions) {
          return actions.order.create({
            purchase_units: [
              {
                description: "Students or Under 18.",
                amount: { currency_code: "EUR", value: 15 },
              },
            ],
          });
        },

        onApprove: function (data, actions) {
          return actions.order.capture().then(function (details) {
            alert(
              "Transaction completed by " + details.payer.name.given_name + "!"
            );
          });
        },

        onError: function (err) {
          console.log(err);
        },
      })
      .render("#paypal-button-container");
  }
  initPayPalButton();
</script> --}}

@elseif ( $_GET[ 'type' ] === 'student' )

<h3>Student / Under 18 - &euro;15</h3>

@include('partials.paypal.student18')

{{-- <div id="smart-button-container">
  <div style="text-align: center">
    <div id="paypal-button-container"></div>
  </div>
</div>
<script src="https://www.paypal.com/sdk/js?client-id=sb&currency=EUR"
        data-sdk-integration-source="button-factory"></script>
<script>
  function initPayPalButton() {
    paypal
      .Buttons({
        style: {
          shape: "rect",
          color: "gold",
          layout: "vertical",
          label: "paypal",
        },

        createOrder: function (data, actions) {
          return actions.order.create({
            purchase_units: [
              {
                description: "Regular subscription",
                amount: { currency_code: "EUR", value: 25 },
              },
            ],
          });
        },

        onApprove: function (data, actions) {
          return actions.order.capture().then(function (details) {
            alert(
              "Transaction completed by " + details.payer.name.given_name + "!"
            );
          });
        },

        onError: function (err) {
          console.log(err);
        },
      })
      .render("#paypal-button-container");
  }
  initPayPalButton();
</script> --}}

@endif
