<article @php post_class() @endphp>
  @include('partials.entry-meta')
  <div class="entry-content">
    {{-- @include('partials.no-translation') --}}
    @php the_content() @endphp
    @include('partials.press-releases.file-download')
  </div>
</article>
