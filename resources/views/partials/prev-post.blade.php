{{-- from App/Single --}}
@if ( $get_the_previous_post && is_single() )
<div class="prev-next-post post-prev">
  <a href="{{ $get_the_previous_post[ 'link' ] }}" rel="bookmark">
    <img src="{{ $get_the_previous_post[ 'thumb' ] }}" alt="{{ $get_the_previous_post[ 'title' ] }}">
    <h2>{{ $get_the_previous_post[ 'title' ] }}</h2>
  </a>
</div>
@endif
