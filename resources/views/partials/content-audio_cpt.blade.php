<article @php post_class() @endphp>
  <header>
    <h2 class="entry-title">{!! the_title() !!}</h2>
  </header>

  <div class="entry-content">{!! the_content() !!}</div>
</article>
