<div class="preloader-wrapper">
  <img class="site-logo small" src="@asset('images/repubblika-logo.png')" alt="Repubblika Logo">
  <div class="box">
    <div class="sk-circle"></div>
    <div class="spinner">
      <div class="rect rect1"></div>
      <div class="rect rect2"></div>
      <div class="rect rect3"></div>
      <div class="rect rect4"></div>
      <div class="rect rect5"></div>
    </div>
  </div>
</div>
