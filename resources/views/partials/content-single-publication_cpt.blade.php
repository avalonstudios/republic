@php
$links = SinglePublicationCpt::prevNextLinks();

$prev = $links[ 'prev' ];
$next = $links[ 'next' ];

$pposts = SinglePublicationCpt::publicationPages();
@endphp

<article @php post_class() @endphp>
  <div class="entry-content">
    @php the_content() @endphp
  </div>

  <footer>
    <div class="publications-navigation">
      @if ($prev)
        <div class="publication-previous">
          {!! $prev !!}
        </div>
      @endif
      @if ($next)
        <div class="publication-next">
          {!! $next !!}
        </div>
      @endif
    </div>
  </footer>
</article>

<nav class="publication-pages">
  <ul class="nav">
    @foreach ($pposts as $ppost)
      <li style={{ $ppost[ 'parent' ] !== 0 ? 'margin-left:10px;' : 'font-weight:700;' }}>
        <a href="{{ $ppost[ 'link' ] }}" rel="bookmark">{{ $ppost[ 'title' ] }}</a>
      </li>
    @endforeach
  </ul>
</nav>
