@php
if ( isset( $eventPostClass ) ) {
  $eventPostClass = $postType;
} else {
  $eventPostClass = '';
}

if ( ! isset( $addID ) ) {
  $addID = get_the_ID();
  $video = get_field( 'video_embed' );
} else {
  $video = get_field( 'video_embed', $addID );
}

$link = get_the_permalink( $addID );
$button = [];
@endphp
<article @php post_class( $eventPostClass ) @endphp>
  <header>
    <h2 class="entry-title"><a href="{{ $link }}" rel="bookmark">{!! get_the_title( $addID ) !!}</a></h2>
    @include('partials.get-post-terms', [ 'postType' => 'video_type', 'addID' => $addID ])
  </header>

  <div class="entry-content">
    @include('partials.video.facebook-embed')
    {{-- <div class="embed-container">{!! $video !!}</div> --}}
  </div>
  @include(
    'components.btn',
    [
      $button[ 'url' ] = $link,
      $button[ 'title' ] = 'watch video',
      $button[ 'target' ] = '',
    ]
  )
</article>
