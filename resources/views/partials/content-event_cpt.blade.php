@php
if ( isset( $eventPostClass ) ) {
  $eventPostClass = $postType;
} else {
  $eventPostClass = '';
}

if ( !isset( $addID ) ) {
  $addID = get_the_ID();
}
@endphp

<article @php post_class( $eventPostClass ) @endphp>
  <header>
    <h1 class="entry-title">{!! get_the_title( $addID ) !!}</h1>
    {{-- <h1 class="entry-title">{!! get_the_title() !!}</h1> --}}
  </header>

  <div class="entry-content">
    @include(
      'partials.events.event-details',
      [
        'displaySingleDates' => true,
        'displayMultipleDates' => false
      ]
    )
  </div>
</article>
