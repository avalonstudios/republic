@php
  if ( ! isset( $videoID ) ) {
    $flds = get_fields();
  } else {
    $flds = get_fields( $videoID );
  }
  $embedType = $flds[ 'yt_fb' ];
@endphp

@switch( $embedType )
  @case( 'yt' )
  @php
    $youtube = $flds[ 'video_embed' ];
  @endphp
  <div class="embed-container">{!! $youtube !!}</div>
  @break

  @case( 'fb' )
    @php
      $fb         = $flds[ 'facebook_embed' ];
      $url        = $fb[ 'facebook_url' ];
      $fullscreen = $fb[ 'allow_full_screen' ];
      $autoplay   = $fb[ 'autoplay' ];
      $lazy       = $fb[ 'lazy_loading' ];
      $width      = $fb[ 'width' ];
      $text       = $fb[ 'show_text' ];
      $captions   = $fb[ 'show_captions' ];
    @endphp

  <div class="embed-container">
    <iframe
      allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"
{{--      height="614"--}}
      src="https://www.facebook.com/plugins/video.php?height=314&href={{ urlencode($url) }}&show_text=false&width=560&t=0"
      style="border:none;overflow:hidden"
{{--      width="560"--}}
       @if ( $fullscreen )
         allowFullScreen="{{ $fullscreen }}"
       @endif
    ></iframe>
    </div>
  @break
@endswitch

{{-- <pre>@dump($flds)</pre> --}}
