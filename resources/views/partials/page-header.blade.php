@php
$postID = get_the_ID();
@endphp

@if ( App::title() )
<div
  class="page-header{{ ! App::getThePostThumbnail( $postID ) ? ' no-image' : '' }}"
  @if (is_single() || is_page())
    @if ( App::getThePostThumbnail( $postID ) )
      style="background-image:url('{{ App::getThePostThumbnail( $postID ) }}')"
    @endif
  @endif
>
  <h1
    @if ( is_single() )
      @if ( $get_the_next_post || $get_the_previous_post )
      class="has-prev-next"
      @endif
    @endif
  >{!! App::title() !!}</h1>

  @if ( is_single() )
    @include('partials.prev-post')
    @include('partials.next-post')
  @endif

  @if ( is_single() && get_post_type() === 'pressrelease_cpt' )
    <div class="pr-number-container">
      <div class="pr-number">{{ pll__( 'Press Release' ) }} PR {{ $get_pr_no }}</div>
    </div>
  @endif
</div>
@else
<div class="header-spacer"></div>
@endif

@if ( $get_custom_category )
<div class="custom-category">
  <i class="fas fa-folder-open"></i> <a href="{{ $get_custom_category[ 'link' ] }}">{{ $get_custom_category[ 'name' ] }}</a>
</div>
@endif

{{-- @php
date_default_timezone_set("Europe/Malta");
echo get_option('gmt_offset');
echo get_option('timezone_string');
@endphp --}}
