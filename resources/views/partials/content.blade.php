<article @php post_class() @endphp>
  @if ( has_post_thumbnail() )
    @php
    $thumb[ 'url' ] = get_the_post_thumbnail_url();
    $thumb[ 'alt' ] = get_the_title();
    @endphp
    <div class="post-thumbnail">
      <a href="{{ the_permalink() }}" rel="bookmark">
        {{ imgResize( $thumb, 300, 300, true ) }}
      </a>
    </div>
  @else
    <a
      href="{{ the_permalink() }}"
      rel="bookmark"
      class="post-thumbnail no-image"
      style="background-image: url('@asset('images/no-image.png')')">
    </a>
  @endif
  <header>
    <h2 class="entry-title">
      <a href="{{ get_permalink() }}">{!! get_the_title() !!}
        {!! is_sticky() ? '<i class="fas fa-paperclip"></i>' : '' !!}</a>
    </h2>
    @include('partials/entry-meta')
  </header>
  <div class="entry-summary">
    @php the_excerpt() @endphp
  </div>
  <footer>
    @include('partials.archive-post-taxonomies')
    @include(
      'components.btn',
      [
        $button[ 'url' ] = get_the_permalink(),
        $button[ 'target' ] = '',
        $button[ 'title' ] = 'read more'
      ]
    )
  </footer>
</article>
