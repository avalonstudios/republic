<div class="header-menu-search hide">
  <div class="search-form-wrapper">
    <div class="search-form-width">
      {{-- {!! get_search_form(false) !!} --}}
      <form role="search" method="get" class="search-form" action="{{ home_url() }}">
        <div class="btn search-close"><i class="fas fa-times"></i></div>
        <label>
          <span class="screen-reader-text">Search for:</span>
          <input list="search-queries" type="search" class="search-field" placeholder="{{ pll__( 'Search' ) }} …" value="" name="s">
        </label>
        <input type="submit" class="btn search-submit" value="{{ pll__( 'Search' ) }}">
      </form>
    </div>
  </div>
</div>
