@php
if ( isset( $prID ) ) {
  $id = $prID;
} else {
  $id = get_the_ID();
}
@endphp

<div class="entry-meta{{ isset( $classes ) ? ' ' . $classes : '' }}">
  <time class="updated" datetime="{{ get_post_time('c', true, $id) }}">
    @if ( pll_current_language() === 'mt' )
      <span class="day">{{ get_the_date('d', $id) }}</span>
      <span class="of">ta'</span>
    @else
      <span class="day">{{ get_the_date('d', $id) }}</span><span class="of">{{ get_the_date('S', $id) }}</span>
    @endif
      <span class="month">{{ pll__( get_the_date('F', $id) ) }}</span>
      <span class="year">{{ get_the_date('Y', $id) }}</span>
  </time>

  @if ( get_the_modified_date('U', $id) > get_the_date('U', $id) )
    <span class="updated-text">{{ pll__( 'updated' ) }}</span>
    @if ( pll_current_language() === 'mt' )
      <time class="modified">
        {{ get_the_modified_date('d', $id) }}/{{ pll__( get_the_modified_date('F', $id) ) }}/{{ get_the_modified_date('Y', $id) }}
      </time>
    @else
      <time class="modified">{{ get_the_modified_date('d/M/Y', $id) }}</time>
    @endif
  @endif

  <p class="byline author vcard">
    {{ pll__('By') }} <a href="{{ get_author_posts_url(get_the_author_meta('ID')) }}" rel="author" class="fn">
      {{ get_the_author() }}
    </a>
  </p>
</div>
