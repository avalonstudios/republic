<header class="banner">
  <div class="container">
    <div class="header-grid">
      <a class="brand" href="{{ home_url('/') }}">{{ get_bloginfo('name', 'display') }}</a>

      <a class="" href="{{ home_url('/') }}">
        <img class="site-logo small" src="@asset('images/repubblika-logo.png')" alt="Repubblika Logo">
        <img class="site-logo large" src="@asset('images/repubblika-logo-large.png')" alt="Repubblika Logo">
      </a>

      @include('menus.main-menu')
      @include(
        'menus.social-menu',
        [
          'classes' => ' social-header hide',
          'socialIcons' => App::getOptions()[ 'social_icons' ][ 'social_icons' ]
        ]
      )
      <div class="hamburger"><i class="fas fa-bars"></i></div>
      <div class="search-menu-btn"><i class="fas fa-search"></i></div>
      @include('partials.searchform')
      <div class="menu-underlay hide"></div>
    </div>
    @include('partials.announcements')
  </div>
</header>
