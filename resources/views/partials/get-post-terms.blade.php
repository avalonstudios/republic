@php
if ( ! isset( $addID ) ) {
  $postID = get_the_ID();
} else {
  $postID = $addID;
}
$theTerms = Single::getThePostTerms( $postID, $postType );
@endphp
<div class="post-terms">
  <div class="terms-icon"><i class="fas fa-bookmark"></i></div>
  @foreach ($theTerms as $term)
  <a class="post-term" href="{{ $term[ 'link' ] }}" rel="bookmark">{{ $term[ 'name' ] }}</a>@if ( ! $loop->last ), @endif
  @endforeach
</div>
