<?php
add_action(
	'init',
	function() {
		# These settings are in common for the 3 hotels
		$settings =     [
                            # Add the post type to the site's main RSS feed:
                            'show_in_feed'      =>  true,
                            'show_in_menu'      =>  'repubblika_menu',
                            'map_meta_cap'      =>  true,
                            'show_in_rest'      =>  false,
                            'hierarchical'      =>  true,
                        ];

		$icon =	        [
                            'menu_icon' => 'dashicons-products',
                        ];

		$capability =	[ 'capability_type' => 'post' ];

		$adminCols =	[
                            'admin_cols'    =>  [
                                                    'photo' =>  [
                                                                    'title'     => 'Photo',
                                                                    'function'  => function() {
                                                                                        $imageURL[ 'url' ] = get_field( 'image' );
                                                                                        echo "<img
                                                                                            src='" . imgResize( $imageURL, 50, 50, true,
                                                                                            true, true ) . "'>";
                                                                                    }
                                                                ]
                                                ]
                        ];

		$supports =     [
                            'supports' =>   [
                                                'title',
                                            ],
                        ];

		# $names, change for each hotel
		$names =	    [
                            # Override the base names used for labels:
                            'singular'          => 'Member',
                            'plural'            => 'Members',
                            'slug'              => 'member'
                        ];

		# register cornucopia post type
        register_extended_post_type( 'member_cpt', $settings + $capability + $supports + $adminCols, $names );

        $names =	    [
                            # Override the base names used for labels:
                            'singular'          => 'Announcement',
                            'plural'            => 'Announcements',
                            'slug'              => 'announcement'
                        ];

		# register cornucopia post type
        register_extended_post_type( 'announcement_cpt', $settings + $capability + $supports, $names );

        $settings =     [
                            # Add the post type to the site's main RSS feed:
                            'show_in_feed'      =>  true,
                            'show_in_menu'      =>  'rep_events_menu',
                            'map_meta_cap'      =>  true,
                            'show_in_rest'      =>  false,
                            'hierarchical'      =>  true,
                        ];

        $names =	    [
                            # Override the base names used for labels:
                            'singular'          => 'Event',
                            'plural'            => 'Events',
                            'slug'              => 'event'
                        ];

        $adminCols =	[
                            'admin_cols'    =>  [
                                                    'start_data' => [
                                                                        'title'         => 'Start Date',
                                                                        'meta_key'      => 'start_date',
                                                                        'date_format'   => 'D, dS M Y @ H:ia'
                                                    ],
                                                    'end_data' => [
                                                                        'title'         => 'End Date',
                                                                        'meta_key'      => 'end_date',
                                                                        'date_format'   => 'D, dS M Y @ H:ia'
                                                                    ]
                                                ]
                        ];

        register_extended_post_type( 'event_cpt', $settings + $capability + $supports + $adminCols, $names );

        $names =	    [
                            # Override the base names used for labels:
                            'singular'          => 'Venue',
                            'plural'            => 'Venues',
                            'slug'              => 'venue'
                        ];

        register_extended_post_type( 'venue_cpt', $settings + $capability + $supports, $names );

        $names =	    [
                            # Override the base names used for labels:
                            'singular'          => 'Speaker',
                            'plural'            => 'Speakers',
                            'slug'              => 'speaker'
                        ];

        register_extended_post_type( 'speaker_cpt', $settings + $capability + $supports, $names );

        $settings =     [
                            # Add the post type to the site's main RSS feed:
                            'show_in_feed'      =>  true,
                            'show_in_menu'      =>  'repubblika_menu',
                            'map_meta_cap'      =>  true,
                            'show_in_rest'      =>  true,
                            'hierarchical'      =>  true,
                        ];

        $names =	    [
                            # Override the base names used for labels:
                            'singular'          => 'Press Release',
                            'plural'            => 'Press Releases',
                            'slug'              => 'press-release'
                        ];

        $supports =     [
                            'supports' =>   [
                                                'title',
                                                'editor',
                                                'thumbnail',
                                                'author',
                                                'excerpt'
                                            ],
                        ];

        register_extended_post_type( 'pressrelease_cpt', $settings + $capability + $supports, $names );

        /**
         * Publications - START
         */
        $names =	    [
                            # Override the base names used for labels:
                            'singular'          => 'Publication',
                            'plural'            => 'Publications',
                            'slug'              => 'publication'
                        ];

        $capability =	[ 'capability_type' => 'page' ];

        $supports =     [
                            'supports' =>   [
                                                'title',
                                                'editor',
                                                'author',
                                                'excerpt',
                                                'page-attributes'
                                            ],
                        ];

        register_extended_post_type( 'publication_cpt', $settings + $capability + $supports, $names );
        /**
         * Publications - END
         */

        $adminCols =	[
                            'admin_cols'    =>  [
                                                    'taxonomy'          =>  [
                                                                                'taxonomy'          =>  'video_type',
                                                                            ],
                                                    'featured_image'    =>  [
                                                                                'title'             => 'Photo',
                                                                                'featured_image'    => 'thumbnail',
                                                                                'width'             => 80,
                                                                                'height'            => 80
                                                                            ]
                                                ]
                        ];

        $supports =     [
                            'supports' =>   [
                                                'title',
                                                'editor',
                                                'thumbnail',
                                                'comments'
                                            ],
                        ];

        $names =	    [
                            # Override the base names used for labels:
                            'singular'          => 'Video',
                            'plural'            => 'Videos',
                            'slug'              => 'video'
                        ];

        register_extended_post_type( 'video_cpt', $settings + $capability + $supports + $adminCols, $names );

        $adminCols =	[
                            'admin_cols'    =>  [
                                                    'taxonomy'          =>  [
                                                                                'taxonomy'          =>  'audio_type',
                                                                            ],
                                                ]
                        ];

        $supports =     [
                            'supports' =>   [
                                                'title',
                                                'editor',
                                            ],
                        ];

        $names =	    [
                            # Override the base names used for labels:
                            'singular'          => 'Audio',
                            'plural'            => 'Audio',
                            'slug'              => 'audio'
                        ];

        register_extended_post_type( 'audio_cpt', $settings + $capability + $supports + $adminCols, $names );

        /**
         * TAXONOMIES
         */

        $settings =     [
                            # Use radio buttons in the meta box in Admin area
                            'meta_box'			=> 'radio',
                            'hierarchical'		=> true
                        ];

        $names =        [
                            'slug'				=> 'event-categories'
                        ];

        register_extended_taxonomy( 'event_type', 'event_cpt', $settings + $names );

        $names =        [
                            'slug'				=> 'video-types'
                        ];

        register_extended_taxonomy( 'video_type', 'video_cpt', $settings + $names );

        $names =        [
                            'slug'				=> 'audio-types'
                        ];

        register_extended_taxonomy( 'audio_type', 'audio_cpt', $settings + $names );

        $names =        [
                            'slug'				=> 'publication-types'
                        ];

        /**
         * Publication Categories
         */

        $settings =     [
                            # Use radio buttons in the meta box in Admin area
                            'meta_box'			=> 'simple',
                            'hierarchical'		=> true
                        ];

        register_extended_taxonomy( 'publication_type', 'publication_cpt', $settings + $names );
	}
);

/** Register custom menu page. */
add_action(
	'admin_menu',
	function() {
		add_menu_page(
			'Repubblika',
			'Repubblika',
			'edit_posts',
			'repubblika_menu',
			'',
			'dashicons-admin-home',
			6
        );

        add_menu_page(
			'Events',
			'Events',
			'edit_posts',
			'rep_events_menu',
			'',
			'dashicons-admin-home',
			6
		);

		add_submenu_page(
			'rep_events_menu',
			'Event Type',
			'Event Type',
			'edit_others_posts',
			'edit-tags.php?taxonomy=event_type&post_type=event_cpt'
        );


		add_submenu_page(
			'repubblika_menu',
			'Video Type',
			'Video Type',
			'edit_others_posts',
			'edit-tags.php?taxonomy=video_type&post_type=video_cpt'
        );

        add_submenu_page(
			'repubblika_menu',
			'Audio Type',
			'Audio Type',
			'edit_others_posts',
			'edit-tags.php?taxonomy=audio_type&post_type=audio_cpt'
        );

        add_submenu_page(
			'repubblika_menu',
			'Publication Type',
			'Publication Type',
			'edit_others_posts',
			'edit-tags.php?taxonomy=publication_type&post_type=publication_cpt'
		);
	}
);
