<?php

namespace App;

/**
 * Add <body> classes
 */
add_filter('body_class', function (array $classes) {
    /** Add page slug if it doesn't exist */
    if (is_single() || is_page() && !is_front_page()) {
        if (!in_array(basename(get_permalink()), $classes)) {
            $classes[] = basename(get_permalink());
        }
    }

    /** Add class if sidebar is active */
    if (display_sidebar()) {
        $classes[] = 'sidebar-primary';
    }

    /** Clean up class names for custom templates */
    $classes = array_map(function ($class) {
        return preg_replace(['/-blade(-php)?$/', '/^page-template-views/'], '', $class);
    }, $classes);

    return array_filter($classes);
});

/**
 * Add "… Continued" to the excerpt
 */
add_filter('excerpt_more', function () {
    return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
});

/**
 * Template Hierarchy should search for .blade.php files
 */
collect([
    'index', '404', 'archive', 'author', 'category', 'tag', 'taxonomy', 'date', 'home',
    'frontpage', 'page', 'paged', 'search', 'single', 'singular', 'attachment', 'embed'
])->map(function ($type) {
    add_filter("{$type}_template_hierarchy", __NAMESPACE__.'\\filter_templates');
});

/**
 * Render page using Blade
 */
add_filter('template_include', function ($template) {
    collect(['get_header', 'wp_head'])->each(function ($tag) {
        ob_start();
        do_action($tag);
        $output = ob_get_clean();
        remove_all_actions($tag);
        add_action($tag, function () use ($output) {
            echo $output;
        });
    });
    $data = collect(get_body_class())->reduce(function ($data, $class) use ($template) {
        return apply_filters("sage/template/{$class}/data", $data, $template);
    }, []);
    if ($template) {
        echo template($template, $data);
        return get_stylesheet_directory().'/index.php';
    }
    return $template;
}, PHP_INT_MAX);

/**
 * Render comments.blade.php
 */
add_filter('comments_template', function ($comments_template) {
    $comments_template = str_replace(
        [get_stylesheet_directory(), get_template_directory()],
        '',
        $comments_template
    );

    $data = collect(get_body_class())->reduce(function ($data, $class) use ($comments_template) {
        return apply_filters("sage/template/{$class}/data", $data, $comments_template);
    }, []);

    $theme_template = locate_template(["views/{$comments_template}", $comments_template]);

    if ($theme_template) {
        echo template($theme_template, $data);
        return get_stylesheet_directory().'/index.php';
    }

    return $comments_template;
}, 100);


add_filter('sage/display_sidebar', function ($display) {
    static $display;

    $fullWidth = get_field( 'full_width' );

    if ( $fullWidth ) {
        return;
    }

    if ( get_post_type() === 'publication_cpt' ) return;

    if ( is_front_page() ) {
        return;
    }

    isset($display) || $display = in_array(true, [
        // The sidebar will be displayed if any of the following return true
        is_single(),
        is_archive(),
        is_page(),
        is_home(),
        is_404(),
        is_page_template('template-custom.php'),
    ]);

    return $display;
});

add_filter( 'pmpro_email_body', function( $temail_body, $instance ) {
	$userID = "Ref. Code: {$_COOKIE[ 'rep_cookie' ]} <br><br>";
	$output = $userID . $temail_body;
	return $output;
}, 10, 2 );

add_filter( 'caldera_forms_render_get_field', function( $field )  {
  if ( $field[ 'slug' ] == 'ref_code' ) {
    $field[ 'config' ][ 'default' ] = $_COOKIE[ 'rep_cookie' ];
  }
  return $field;
});


add_filter( 'get_the_archive_title', function( $title ) {

	if ( is_category() ) {

        $title = single_cat_title( '', false );

	} elseif ( is_tag() ) {

		$title = single_tag_title( '', false );

	} elseif ( is_author() ) {

		$title = '<span class="vcard">' . get_the_author() . '</span>';

	} elseif ( is_post_type_archive() ) {

        $post_type = get_query_var( 'post_type' );
        $post_type_obj = get_post_type_object( $post_type );
        $ttl = apply_filters( 'post_type_archive_title', $post_type_obj->labels->name, $post_type );
        $name = pll__( 'Archives' ). ': ';
        $title = $name . pll__( $ttl );

		//$title = post_type_archive_title( $name, false );

	} elseif ( is_tax() ) {

        switch ( get_queried_object()->taxonomy ) {
            case 'audio_type':
                $title = pll__('Audio Type') . ': ' . single_term_title( '', false );
                break;

            case 'video_type':
                $title = pll__('Video Type') . ': ' . single_term_title( '', false );
                break;

            case 'event_type':
                $title = pll__('Event Type') . ': ' . single_term_title( '', false );
                break;

            case 'news_type':
                $title = pll__('News Type') . ': ' . single_term_title( '', false );
                break;

            default:
                # code...
                break;
        }
	}

	return $title;

}, 99, 1 );

add_filter('caldera_forms_manage_cap', function( $cap ) {

    $cap = 'edit_caldera_forms';

    return $cap;
}, 10, 1);


add_filter( 'the_content', function( $content ) {

    $noTrans = \Single::getNoTranslation();

    if ( ! isset( $noTrans ) ) {
        return $content;
    }

    $text = '';

    if ( $noTrans[ 'no_tx' ] ) :
    $text .= "<div class='no-translation'>";
    $text .= $noTrans[ 'msg' ];
    $text .= "<ul class='no-translation-languages'>";
    $text .= $noTrans[ 'menu' ];
    $text .= "</ul></div>";
    endif;


    // Check if we're inside the main loop in a single Post.
    if ( is_singular() && in_the_loop() && is_main_query() ) {
        return $content . $text;
    }

    return $content;
}, 1 );

// add_filter('acf/settings/show_admin', '__return_false');
