<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class TemplateCommitteeMembers extends Controller
{
    public function members() {
        $args = [
            'post_type' => 'member_cpt',
        ];

        return get_posts( $args );
    }
}
