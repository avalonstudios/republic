<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class App extends Controller
{
    public static function title()
    {
        $pageTitle = get_field('hide_page_title');
        if ($pageTitle) {
            return;
        }

        if (is_singular('member_cpt')) {
            return;
        }
        if (is_home()) {
            if ($home = get_option('page_for_posts', true)) {
                return get_the_title($home);
            }
            return __('Latest Posts', 'sage');
        }
        if (is_archive()) {
            return get_the_archive_title();
        }
        if (is_search()) {
            return sprintf(__('Search Results for %s', 'sage'), get_search_query());
        }
        if (is_404()) {
            return __('Not Found', 'sage');
        }
        return get_the_title();
    }

    public static function getOptions()
    {
        return get_fields('options');
    }

    public static function getThePostThumbnail($id, $w = 1250, $h = 400)
    {
        if (is_singular('pressrelease_cpt') || is_post_type_archive('pressrelease_cpt')) {
            $pressReleaseImage = get_fields('options')['press_releases'];
            if (has_post_thumbnail($id)) {
                $thumb = get_the_post_thumbnail_url($id);
            } else {
                $thumb = $pressReleaseImage;
            }

        } else {
            $thumb = get_the_post_thumbnail_url($id);
        }

        if (!$thumb) {
            return;
        }

        $thumb = aq_resize($thumb, $w, $h, true, true, true);
        return $thumb;
    }

    public static function shorten($str, $length)
    {
        if ($str) {
            $string = substr($str, 0, $length) . '...';
            return $string;
        }
    }

    public static function getThePostTaxonomies()
    {
        $taxonomies = App::getTaxonomies();

        $categoriesAndTags = [];

        if ($taxonomies) {
            foreach ($taxonomies as $taxes) {
                if (!$taxes) {
                    continue;
                }
                $term_id = $taxes->term_id;
                $name = $taxes->name;
                $slug = $taxes->slug;
                $taxonomy = $taxes->taxonomy;

                if ($taxonomy === 'category') {
                    $categoriesAndTags['categories'][] =
                        [
                            'term_id' => $term_id,
                            'name' => $name,
                            'slug' => $slug,
                            'taxonomy' => $taxonomy,
                            'link' => get_term_link($term_id, $taxonomy)
                        ];
                } elseif ($taxonomy === 'post_tag') {
                    $categoriesAndTags['post_tag'][] =
                        [
                            'term_id' => $term_id,
                            'name' => $name,
                            'slug' => $slug,
                            'taxonomy' => $taxonomy,
                            'link' => get_term_link($term_id, $taxonomy)
                        ];
                }
            }

            if (empty($categoriesAndTags['categories'])) {
                $categoriesAndTags['categories'] = false;
            }

            if (empty($categoriesAndTags['post_tag'])) {
                $categoriesAndTags['post_tag'] = false;
            }
        }

        return $categoriesAndTags;
    }

    private static function getTaxonomies()
    {
        $taxonomies = [];
        $categories = get_the_category(get_the_ID());
        $tags = get_the_tags(get_the_ID());
        $taxonomies = [
            'categories' => $categories,
            'tags' => $tags
        ];

        return collect($taxonomies)->flatten();
    }

    public static function getPrNos($id)
    {
        return get_field('pr_no', $id);
    }

    public function siteName()
    {
        return get_bloginfo('name');
    }

    public function getCustomCategory()
    {
        if (is_singular('event_cpt')) {
            $cat = get_field('category');
            $name = $cat->name;
            $link = get_term_link($cat->term_id, $cat->taxonomy);
            $category = [
                'name' => $name,
                'link' => $link
            ];

            return $category;
        }
    }

    public function getAnnouncements(): array
    {
        $now = date('U');
        $timezone = new \DateTime('Europe/Malta');
        $offset = $timezone->getOffset();

        $annArray = [];

        $args = [
            'post_type' => 'announcement_cpt',
            'posts_per_page' => -1
        ];

        $ann_qry = new \WP_Query($args);

        if ($ann_qry->have_posts()) {
            while ($ann_qry->have_posts()) {
                $ann_qry->the_post();
                $flds = get_fields();
                $startDate = $flds['start_date'];
                $endDate = $flds['end_date'];
                $endDate = strtotime('-2 hours', $endDate);
                $announcement = $flds['announcement'];
                $title = get_the_title();


                $output = [
                    'title' => $title,
                    'content' => $announcement
                ];

                if (($startDate < $now) && ($endDate > $now)) {
                    $annArray[] = $output;
                }
            }

            wp_reset_query();
        }

        return $annArray;
    }

    public function getLanguage()
    {
        return get_locale();
    }

    public function getPrNo()
    {
        if (get_post_type() !== 'pressrelease_cpt') {
            return;
        }

        return get_field('pr_no');
    }
}
