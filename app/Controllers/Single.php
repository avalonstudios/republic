<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class Single extends Controller
{
    public function getThePreviousPost() {
        $prevPost   = get_previous_post();
        if ( ! $prevPost ) {
            return;
        }

        $prevID     = $prevPost->ID;
        $prevTitle  = $prevPost->post_title;
        $prevLink   = get_the_permalink( $prevID );
        $prevThumb  = get_the_post_thumbnail_url( $prevID );

        if ( $prevThumb ) {
            $prevThumb = aq_resize( $prevThumb, 50, 50, true );
        } else {
            $prevThumb = get_template_directory_uri() . '/assets/images/no-image-sm.png';
        }

        $arr =  [
                    'title' => $prevTitle,
                    'link'  => $prevLink,
                    'thumb' => $prevThumb
                ];
        return $arr;
    }

    public function getTheNextPost() {
        $nextPost   = get_next_post();
        if ( ! $nextPost ) {
            return;
        }

        $nextID     = $nextPost->ID;
        $nextTitle  = $nextPost->post_title;
        $nextLink   = get_the_permalink( $nextID );
        $nextThumb  = get_the_post_thumbnail_url( $nextID );

        if ( $nextThumb ) {
            $nextThumb = aq_resize( $nextThumb, 100, 100, true );
        } else {
            $nextThumb = get_template_directory_uri() . '/assets/images/no-image-sm.png';
        }

        $arr =  [
                    'title' => $nextTitle,
                    'link'  => $nextLink,
                    'thumb' => $nextThumb
                ];
        return $arr;
    }

    private function getTaxonomies() {
        $taxonomies =   [];
        $categories =   get_the_category( get_the_ID() );
        $tags       =   get_the_tags( get_the_ID() );
        $taxonomies =   [
                            'categories'    => $categories,
                            'tags'          => $tags
                        ];

        $taxonomies = collect( $taxonomies )->flatten();

        return $taxonomies;
    }

    public function getThePostTaxonomies() {
        $taxonomies = $this->getTaxonomies();

        $categoriesAndTags = [];

        if ( $taxonomies ) {
            foreach ( $taxonomies as $taxes ) {
                if ( ! $taxes ) {
                    continue;
                }
                $term_id              = $taxes->term_id;
                $name                 = $taxes->name;
                $slug                 = $taxes->slug;
                $taxonomy             = $taxes->taxonomy;

                if ( $taxonomy === 'category' ) {
                    $categoriesAndTags[ 'categories' ][] =
                        [
                            'term_id'   => $term_id,
                            'name'      => $name,
                            'slug'      => $slug,
                            'taxonomy'  => $taxonomy,
                            'link'      => get_term_link($term_id, $taxonomy)
                        ];
                } elseif ( $taxonomy === 'post_tag' ) {
                        $categoriesAndTags[ 'post_tag' ][] =
                        [
                            'term_id'   => $term_id,
                            'name'      => $name,
                            'slug'      => $slug,
                            'taxonomy'  => $taxonomy,
                            'link'      => get_term_link($term_id, $taxonomy)
                        ];
                }
            }
        }

        return $categoriesAndTags;
    }

    public static function getThePostTerms( $id, $taxonomy ) {
        $theTerms = wp_get_post_terms( $id, $taxonomy );

        $termsArray = [];
        foreach ( $theTerms as $term ) {
            $termsArray[] = [
                'name' => $term->name,
                'link' => get_term_link($term->term_id, $taxonomy)
            ];
        }
        return $termsArray;
    }

    public function getRelatedPosts() {
        $taxonomies = $this->getTaxonomies();

        $taxArray = [];

        foreach ( $taxonomies as $taxes ) {
            if ( ! $taxes ) {
                continue;
            }
            $taxArray[ $taxes->taxonomy ][] = $taxes->term_id;
        }

        if ( isset( $taxArray[ 'post_tag' ] ) ) {
            $postTag = $taxArray[ 'post_tag' ];
        } else {
            $postTag = '';
        }

        if ( isset( $taxArray[ 'category' ] ) ) {
            $postCategory = $taxArray[ 'category' ];
        } else {
            $postCategory = '';
        }

        $args =
            [
                'post_type'         => 'post',
                'posts_per_page'    => 3,
                'tag__in'           => $postTag,
                'post__not_in'      => [ get_the_ID() ],
                'category_in'       => $postCategory,
            ];

        $pposts = get_posts( $args );
        $postsArr = [];

        foreach ( $pposts as $ppost ) {
            $postsArr[] =
                [
                    'title' => $ppost->post_title,
                    'image' => aq_resize( get_the_post_thumbnail_url( $ppost->ID ), 300, 300, true, true, true ),
                    'link'  => get_the_permalink( $ppost->ID ),
                    'date'  => get_the_date( 'U', $ppost->ID ),
                    'modified_date' => get_the_modified_date( 'U', $ppost->ID )
                ];
        }

        return $postsArr;
    }

    public static function getNoTranslation() {
        if ( is_admin() ) return;

        $theFields = get_fields();
        $messages = get_fields( 'options' )[ 'no_translation_messages' ][ 'no_translations_messages' ];

        if ( !isset( $theFields[ 'no_translation' ] ) ) {
            return;
        }
        $args = [
            'hide_current' => true,
            'echo' => 0,
            'show_flags' => 1
        ];
        $currentMsg = 'msg_' . pll_current_language();
        $noTrans = [
            'current_lang'  => pll_current_language(),
            'no_tx'         => $theFields[ 'no_translation' ],
            'msg'           => $messages[ $currentMsg ],
            'menu'          => pll_the_languages( $args )
        ];

        return $noTrans;
    }
}
