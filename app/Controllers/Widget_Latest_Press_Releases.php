<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class Widget_Latest_Press_Releases extends Controller
{
    public static function latestPressReleases( $numOfPosts = 5 ) {
        $args = [
                    'post_type'         => 'pressrelease_cpt',
                    'posts_per_page'    => $numOfPosts
                ];

        $prQry = new \WP_Query($args);

        $postsArray = [];

        if ( $prQry->have_posts() ) {
            while ( $prQry->have_posts() ) { $prQry->the_post();
                $pressReleaseImage = get_fields( 'options' )[ 'press_releases' ];
                if ( has_post_thumbnail() ) {
                    $thumb = get_the_post_thumbnail_url();
                } else {
                    $thumb = $pressReleaseImage;
                }
                $thumb = aq_resize( $thumb, 100, 100, true, true, true );
                $postsArray[] = [
                    'id'        => get_the_ID(),
                    'title'     => get_the_title(),
                    'link'      => get_the_permalink(),
                    'excerpt'   => App::shorten( get_the_excerpt(), 50 ),
                    'thumb'     => $thumb
                ];
            }
        }

        // $postsArray = collect( $postsArray );

        wp_reset_query();

        return $postsArray;
    }
}
