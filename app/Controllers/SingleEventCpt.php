<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class SingleEventCpt extends Controller
{
    public static function getMsg() {
        $flds = get_fields();
        $currentMsg = 'msg_' . pll_current_language();
        $msg = [
            'live_event'    => $flds[ 'live_event' ],
            'msg'           => $flds[ 'messages_by_language' ][ $currentMsg ],
        ];

        return $msg;
    }
}
