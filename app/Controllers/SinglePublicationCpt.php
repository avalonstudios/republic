<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class SinglePublicationCpt extends Controller
{
    public static function prevNextLinks() {
        $prevLink = get_previous_post_link('&laquo;	%link', '%title', true, ' ', 'publication_type');
        $nextLink = get_next_post_link('%link &raquo;', '%title', true, ' ', 'publication_type');

        $output = [
            'prev' => $prevLink,
            'next' => $nextLink
        ];

        return $output;
    }

    public static function publicationPages() {
        $args = [
            'post_type' => 'publication_cpt',
            'order'     => 'ASC',
            'posts_per_page' => -1
        ];

        $pposts = get_posts( $args );

        $postArr = [];

        foreach ($pposts as $ppost) {
            $link = get_the_permalink( $ppost->ID );
            $postArr[] = [
                'title' => $ppost->post_title,
                'link' => $link,
                'parent' => $ppost->post_parent
            ];
        }

        return $postArr;
    }
}
