<?php

pll_register_string( 'repubblika', 'read more', 'Repubblika' );
pll_register_string( 'repubblika', 'Event Type', 'Repubblika' );

pll_register_string( 'repubblika', 'website', 'Repubblika' );
pll_register_string( 'repubblika', 'Admittance', 'Repubblika' );
pll_register_string( 'repubblika', 'Tickets', 'Repubblika' );
pll_register_string( 'repubblika', 'phone', 'Repubblika' );
pll_register_string( 'repubblika', 'Venue', 'Repubblika' );
pll_register_string( 'repubblika', 'Details', 'Repubblika' );
pll_register_string( 'repubblika', 'Other Dates', 'Repubblika' );
pll_register_string( 'repubblika', 'Announcements', 'Repubblika' );
pll_register_string( 'repubblika', 'Search', 'Repubblika' );

pll_register_string( 'repubblika', 'All Videos', 'Repubblika' );
pll_register_string( 'repubblika', 'watch video', 'Repubblika' );

pll_register_string( 'repubblika', 'close', 'Repubblika' );

pll_register_string( 'repubblika', 'back to news list', 'Repubblika' );

pll_register_string( 'repubblika', 'Entrance', 'Repubblika' );
pll_register_string( 'repubblika', 'Price', 'Repubblika' );
pll_register_string( 'repubblika', 'Dates &amp; Times', 'Repubblika' );
pll_register_string( 'repubblika', 'View Events', 'Repubblika' );
pll_register_string( 'repubblika', 'Event Details', 'Repubblika' );

pll_register_string( 'repubblika', 'Executive Committee Members', 'Repubblika' );
pll_register_string( 'repubblika', 'profile', 'Repubblika' );
pll_register_string( 'repubblika', 'members', 'Repubblika' );

pll_register_string( 'repubblika', 'press release', 'Repubblika' );
pll_register_string( 'repubblika', 'Press Release', 'Repubblika' );

pll_register_string( 'repubblika', 'Article No.', 'Repubblika' );

pll_register_string( 'repubblika', 'All Ages', 'Repubblika' );
pll_register_string( 'repubblika', 'All Ages/Licensed', 'Repubblika' );

pll_register_string( 'repubblika', 'Add to Calendar', 'Repubblika' );
pll_register_string( 'repubblika', 'updated', 'Repubblika' );
pll_register_string( 'repubblika', 'download', 'Repubblika' );
pll_register_string( 'repubblika', 'view', 'Repubblika' );

pll_register_string( 'repubblika', 'Categories', 'Repubblika' );
pll_register_string( 'repubblika', 'Tags', 'Repubblika' );
pll_register_string( 'repubblika', 'Related Posts', 'Repubblika' );

pll_register_string( 'repubblika', 'Archives', 'Repubblika' );
pll_register_string( 'repubblika', 'Press Releases', 'Repubblika' );

pll_register_string( 'repubblika', 'Bank Name', 'Repubblika' );
pll_register_string( 'repubblika', 'Account Name', 'Repubblika' );
pll_register_string( 'repubblika', 'Account Number', 'Repubblika' );
pll_register_string( 'repubblika', 'IBAN', 'Repubblika' );
pll_register_string( 'repubblika', 'Copied', 'Repubblika' );
pll_register_string( 'repubblika', 'copy', 'Repubblika' );
pll_register_string( 'repubblika', 'By', 'Repubblika' );

// Custom Post Types slugs
pll_register_string( 'repubblika', 'press-release', 'cpt-slugs' );


/**
 * Month
 */

pll_register_string( 'repubblika', 'January', 'Months' );
pll_register_string( 'repubblika', 'February', 'Months' );
pll_register_string( 'repubblika', 'March', 'Months' );
pll_register_string( 'repubblika', 'April', 'Months' );
pll_register_string( 'repubblika', 'May', 'Months' );
pll_register_string( 'repubblika', 'June', 'Months' );
pll_register_string( 'repubblika', 'July', 'Months' );
pll_register_string( 'repubblika', 'August', 'Months' );
pll_register_string( 'repubblika', 'September', 'Months' );
pll_register_string( 'repubblika', 'October', 'Months' );
pll_register_string( 'repubblika', 'November', 'Months' );
pll_register_string( 'repubblika', 'December', 'Months' );

/**
 * Days
 */

pll_register_string( 'repubblika', 'Mon', 'Days' );
pll_register_string( 'repubblika', 'Tue', 'Days' );
pll_register_string( 'repubblika', 'Wed', 'Days' );
pll_register_string( 'repubblika', 'Thu', 'Days' );
pll_register_string( 'repubblika', 'Fri', 'Days' );
pll_register_string( 'repubblika', 'Sat', 'Days' );
pll_register_string( 'repubblika', 'Sun', 'Days' );

/**
 * Post Types
 */

pll_register_string( 'repubblika', 'Members', 'Post Type Titles' );
pll_register_string( 'repubblika', 'Events', 'Post Type Titles' );
pll_register_string( 'repubblika', 'Venues', 'Post Type Titles' );
pll_register_string( 'repubblika', 'Speakers', 'Post Type Titles' );
pll_register_string( 'repubblika', 'Videos', 'Post Type Titles' );

/**
 * Taxonomies
 */

pll_register_string( 'repubblika', 'Audio Type', 'Taxonomy Titles' );
pll_register_string( 'repubblika', 'Video Type', 'Taxonomy Titles' );
pll_register_string( 'repubblika', 'Event Type', 'Taxonomy Titles' );
pll_register_string( 'repubblika', 'News Type', 'Taxonomy Titles' );
